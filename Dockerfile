FROM python:3.10.13-slim

LABEL version="0.0.1"
LABEL maintainer="livinglab"

ADD calendar_generator.py /
ADD 2022.csv /
ADD 2023.csv /
ADD requirements.txt /
RUN python3 -m pip install -r requirements.txt

CMD [ "python", "./calendar_generator.py" ]
