# Yearly Overview PDF Calendar

All credits goes to so-tourist for this nice snippet of code: https://stackoverflow.com/a/61277350/8284356

### How to use:
Open the python file and adjust the `years` parameter according to your need:

e.g., `years = [2022, 2023]`

Then create the data files with the name of relevant year in the same directory as the python script (e.g., `2022.csv` and `2023.csv`) using the following format:

`|TYPE|FROM|TO|EVENT NAME|COMMENTS|`

`|🚌|17.01|18.01| HPC matter | HPC team |`


⚠ Although it is a CSV file, the expected separator is '|' (for MD compatibility).

Three types are predefined: 🖥, 👩‍🏫, 🚌. Change/modify them [here](calendar_generator.py?ref_type=heads#L15) if neccessary.

Then execute the python script. Resulting PDFs will be available in the same directory.

![Alt text](image.png)
