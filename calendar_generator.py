## All credits goes to https://stackoverflow.com/a/61277350/8284356

import numpy as np
import pandas as pd
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon

# Settings
years = [2022, 2023]
weeks = [1, 2, 3, 4, 5, 6]
days = ['M', 'T', 'W', 'T', 'F', 'S', 'S']
month_names = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August',
               'September', 'October', 'November', 'December']
event_colors = {"🖥":1, "🚌":2, "👩\u200d🏫":3}


def generate_data(year):
    
    dateparse = lambda x: datetime.strptime(x, '%d.%m').replace(year=year) # %Y-%m-%d
    events_raw = pd.read_csv('{}.csv'.format(year), names=['Blank1', 'Type','From','To','Description','Contributor','Blank2'],
                         parse_dates=['From', 'To'], date_parser=dateparse,
                         sep='|')
    events_raw.drop(['Blank1','Blank2'], axis=1, inplace=True)


    #### Events_Expanded should contain one row per day of events
    events_expanded = pd.DataFrame(columns=['Type','Date','Description','Contributor'])
    for ind in events_raw.index:
        for i in  range((events_raw['To'][ind] - events_raw['From'][ind]).days + 1):
            new_row = pd.DataFrame([[events_raw['Type'][ind],(events_raw['From'][ind] + timedelta(days=i)),events_raw['Description'][ind],events_raw['Contributor'][ind]]],
                                   columns=['Type','Date','Description','Contributor'])
            events_expanded = pd.concat([events_expanded,new_row], axis=0, ignore_index=True)

    events = events_expanded

    events['DayOfYear'] = [d.timetuple().tm_yday - 1 for d in events['Date']]
    #day_of_year = events.iloc[0]['Date'].timetuple().tm_yday - 1 # returns 0 for January 1st
    
    idx = pd.date_range('{}-01-01'.format(year), periods=365, freq='D')
    return events, events_raw, pd.Series(range(len(idx)), index=idx)


def split_months(events, df, year):
    """
    Take a df, slice by year, and produce a list of months,
    where each month is a 2D array in the shape of the calendar
    :param df: dataframe or series
    :return: matrix for daily values and numerals
    """
    df = df[df.index.year == year]


    # Empty matrices
    a = np.empty((6, 7))
    a[:] = np.nan

    day_nums = {m:np.copy(a) for m in range(1,13)}  # matrix for day numbers
    day_vals = {m:np.copy(a) for m in range(1,13)}  # matrix for day values
    day_cols = {m:np.copy(a) for m in range(1,13)}  # matrix for descrit day colors

    # Logic to shape datetimes to matrices in calendar layout
    for d in df.items():  # use iterrows if you have a DataFrame

        day = d[0].day
        month = d[0].month
        col = d[0].dayofweek

        if d[0].is_month_start:
            row = 0

        day_nums[month][row, col] = day  # day number (0-31)
##        day_vals[month][row, col] = d[1] # : day value (the heatmap data)
        events_of_the_day = events.loc[events['DayOfYear'] == d[1]]
        day_vals[month][row, col] = 0
        day_cols[month][row, col] = 0
        for event in events_of_the_day.values:
            day_vals[month][row, col] += 1
            day_cols[month][row,col] = event_colors[event[0]]
        

        if col == 6:
            row += 1

    return day_nums, day_vals, day_cols


def create_year_calendar(events_raw, day_nums, day_vals, day_cols, year):
    fig, ax = plt.subplots(3, 4, figsize=(14.85, 10.5))

    maxVal = max([np.amax(np.nan_to_num(day_vals[m])) for m in day_vals]) # finding the max daily number of events
    
    for i, axs in enumerate(ax.flat):

        # Heatmap view
        #axs.imshow(day_vals[i+1], cmap='tab10', vmin=1, vmax=maxVal)
        
        # or Categorical view
        axs.imshow(day_cols[i+1], cmap='tab10', vmin=1, vmax=len(event_colors)+1, alpha=np.nan_to_num(day_vals[i+1])/maxVal)
        
        axs.set_title(month_names[i])

        # Labels
        axs.set_xticks(np.arange(len(days)))
        axs.set_xticklabels(days, fontsize=10, fontweight='bold', color='#555555')
        axs.set_yticklabels([])

        # Tick marks
        axs.tick_params(axis=u'both', which=u'both', length=0)  # remove tick marks
        axs.xaxis.tick_top()

        # Modify tick locations for proper grid placement
        axs.set_xticks(np.arange(-.5, 6, 1), minor=True)
        axs.set_yticks(np.arange(-.5, 5, 1), minor=True)
        axs.grid(which='minor', color='w', linestyle='-', linewidth=2.1)

        # Despine
        for edge in ['left', 'right', 'bottom', 'top']:
            axs.spines[edge].set_color('#FFFFFF')

        # Annotate
        for w in range(len(weeks)):
            for d in range(len(days)):
                day_val = day_vals[i+1][w, d]
                day_num = day_nums[i+1][w, d]

                # Value label
                axs.text(d, w+0.3, f"{day_val:0.0f}",
                         ha="center", va="center",
                         fontsize=10, color="w", alpha=0.8)

                # If value is 0, draw a grey patch
                if day_val == 0:
                    patch_coords = ((d - 0.5, w - 0.5),
                                    (d - 0.5, w + 0.5),
                                    (d + 0.5, w + 0.5),
                                    (d + 0.5, w - 0.5))

                    square = Polygon(patch_coords, fc='#DDDDDD')
                    axs.add_artist(square)

                # If day number is a valid calendar day, add an annotation
                if not np.isnan(day_num):
                    axs.text(d+0.45, w-0.31, f"{day_num:0.0f}",
                             ha="right", va="center",
                             fontsize=6, color="#003333", alpha=0.8)  # day

                # Aesthetic background for calendar day number
                patch_coords = ((d-0.05, w-0.5),
                                (d+0.5, w-0.5),
                                (d+0.5, w+0.05))

                triangle = Polygon(patch_coords, fc='w', alpha=0.7)
                axs.add_artist(triangle)

    # Final adjustments
    fig.suptitle('Living Lab @ Dresden in {}'.format(year), fontsize=16)
    plt.subplots_adjust(left=0.04, right=0.96, top=0.88, bottom=0.04)
    
    events_raw['From'] = events_raw['From'].dt.strftime('%d-%b')
    events_raw['To'] = events_raw['To'].dt.strftime('%d-%b')
    legend_label = ["{} to {} {}".format(t[0],t[1],t[2]) if t[0]!=t[1] else "{}{}{}".format(t[0],11*' ',t[2]) for t in events_raw[["From","To","Description"]].values.tolist()]
    #legend_handle = [event_colors[t] for t in events_raw['Type']]
    plt.legend(legend_label, 
               loc='upper right', bbox_to_anchor=(2.7, 4),
               fancybox=False, shadow=False, ncol=1,
               prop={'family': 'DejaVu Sans Mono'},
               handletextpad = -2)

    # Save to file
    plt.savefig('/tmp/calendar_{}.pdf'.format(year), bbox_inches="tight")
    
    return legend_label


for year in years:
    events, events_raw, df = generate_data(year)
    day_nums, day_vals, day_cols = split_months(events, df, year)
    legend_label = create_year_calendar(events_raw, day_nums, day_vals, day_cols, year)
